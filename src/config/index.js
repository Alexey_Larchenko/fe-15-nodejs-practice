import dotenv from 'dotenv';

process.env.NODE_ENV = process.env.NODE_ENV || 'development';

const envFound = dotenv.config({path: '.env.develop'});

if (!envFound) {
    throw new Error(`Couldn't find .env file`);
}

export default {
    /**
     * @desc System port
     * @type {Number}
     */
    PORT: process.env.PORT,

    /**
     * @desc Developer name
     * @type {String}
     */

    DEV_ID: process.env.DEV_NAME
}