import routes from '../api';

export default ({app}) => {
    app.set('view engine', 'pug');
    app.set('views', 'src/views');

    app.use('/', routes());
}