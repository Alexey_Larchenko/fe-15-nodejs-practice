import { Router } from 'express';
import homeRoute from './routes/homeRoute';
import productRoute from "./routes/productRoute";
import sendGreetingRoute from "./routes/sendGreetingRoute";

export default () => {
    const app = Router();

    homeRoute(app);
    productRoute(app);
    sendGreetingRoute(app)

    return app;
}