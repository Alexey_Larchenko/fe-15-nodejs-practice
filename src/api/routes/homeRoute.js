import {Router} from 'express';
import config from '../../config'

const route = Router();

export default app => {
    app.use('/api', route);

    route.get('/', (request, response) => {
        response.render('index', {
            friends: 0
        });
    });

    route.get('/who-are-you', (request, response) => {
        response.send(config.DEV_ID);
    });
}