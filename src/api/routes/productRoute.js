import {Router} from 'express';

const route = Router();

export default app => {
    app.use('/product', route);

    route.get('/', (request, response) => {
        response.send('Test string for product');
    });
}