import {Router} from 'express';

const route = Router();

export default app => {
    app.use('/mail', route);

    route.post('/welcome-new-dev', (request, response) => {
        try {
            var api_key = 'f3240f8130488e20378cd5c6fd5ad080-cb3791c4-3f3847d8';
            var domain = 'sandbox6f96a3bc68ce4a7ca8df7113c661db6d.mailgun.org';
            var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

            var data = {
                from: 'Excited User <me@samples.mailgun.org>',
                to: 'shimanuchi@gmail.com',
                subject: 'Hello Alex, nice to meet u ♥',
                text: 'Testing some Mailgun awesomeness!'
            };

            mailgun.messages().send(data, function (error, body) {
                console.log(body);
            });

            return response.json({
                success: true,
            });
        } catch (err) {
            return response.json({
                success: true,
                error: err.getMessage()
            });
        }
    });
}